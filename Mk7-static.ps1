$EthPresent = (Get-NetAdapter -InterfaceDescription *AX8877*).InterfaceIndex

if ( $EthPresent -eq $null )
{
    Write-Host 'No ASIX 8877x based Ethernet adapter seems to be present in the system.'
    Write-Host -NoNewLine 'Press Enter to exit script...'
    $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
    Exit
}

$EthIfArray = @((Get-NetAdapter -InterfaceDescription *AX8877*).InterfaceIndex)

if ( $EthIfArray.count -gt 1 )
{
    Write-Host 'There seems to be more than one ASIX 8877x based Ethernet adapter in the system.'
    Write-Host -NoNewLine 'This script will not handle that. Press Enter to exit script...'
    $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
    Exit
}

$EthIfIndex = (Get-NetAdapter -InterfaceDescription *AX8877*).InterfaceIndex

$WfpIpAddr = (Get-NetIPAddress -AddressFamily IPv4 -InterfaceIndex $EthIfIndex).IPAddress

$IpAddrArray = $WfpIpAddr.Split(".")
$IpAddrRange = $IpAddrArray[0] + "." + $IpAddrArray[1] + "." + $IpAddrArray[2]

if ( $IpAddrRange -notmatch "172.16.42" )
{
    Write-Host 'The IP range used does not match what the Pineapple uses.'
    Write-Host -NoNewLine 'This script will not handle that. Press Enter to exit script...'
    $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
    Exit
}

Remove-NetIPAddress -InterfaceIndex $EthIfIndex -AddressFamily IPv4 -IPAddress $WfpIpAddr -Confirm:$false
New-NetIPAddress -InterfaceIndex $EthIfIndex -IPAddress 172.16.42.42 -PrefixLength 16 -Confirm:$false
Set-DnsClientServerAddress -InterfaceIndex $EthIfIndex -ServerAddresses ("8.8.8.8","8.8.4.4")
