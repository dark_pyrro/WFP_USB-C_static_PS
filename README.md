NOTE! You are using the contents of this repo at your own risk!  
  

The PowerShell script in this repo helps to set the static IP address of 172.16.42.42 of the
Windows PC that is connected to the Hak5 WiFi Pineapple Mark VII using its USB-C based Ethernet interface.  
This is similar to setting it all manually as per the documentation for the Mark VII  
https://docs.hak5.org/wifi-pineapple/setup/connecting-to-the-wifi-pineapple-on-windows
  
To use the PowerShell script, the current (logged in) user needs to be local admin (i.e. member of the Administrators local group)  

Create the ps1 file in the Documents directory (not OneDrive\Documents) of the current user  

It's of course possible to use any directory on the Windows box, just adjust the path to the ps1 file when running PowerShell  

Press the Windows key + r (a.k.a. `GUI r` if speaking "Ducky Script")  

Enter the following  
`powershell -Executionpolicy Bypass %HOMEPATH%\Documents\Mk7-static.ps1`  
or (if wanting to read the final output)  
`powershell -NoExit -Executionpolicy Bypass %HOMEPATH%\Documents\Mk7-static.ps1`  

Press:  
Ctrl + Shift + Enter  
Select "Yes" (or whatever is shown if the OS language isn't English)  

Let the PowerShell script run  

Run  
`ipconfig /all`  
to verify that the settings has changed (there should also be an output from the script)  

It's also possible to check that the change was successful using the Settings/Control Panel  
`GUI r`  
Enter the following and press enter  
`ncpa.cpl`  

Payload files for some Hak5 gear is also available in the case that a Hak5 device should do the job (instead of doing it manually)  

For the Key Croc, use the file  
`payload_croc.txt`  
Remember to set the `DUCKY_LANG` and put the ps1 file in the correct location on the device  

For the Bash Bunny, use the file  
`payload.txt`  
Remember to set the `DUCKY_LANG` and put the ps1 file in the correct location on the device  

For the USB Rubber Ducky Gen 2, enter the contents in the file  
`payload_to_encode_with_payloadstudio.txt`  
in PayloadStudio and encode the payload, remember to set the correct language for the target
and put the ps1 file in the correct location on the device
